import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Warehouse {
    private Map<String, ProductList> products;

    public Warehouse() {
        products = new HashMap<>();
    }

    // S O L I D
    public void addProduct(int quantity, Product product) {
        if (!products.containsKey(product.getName())) {
            // jeśli nie posiadamy listy produktów o tej nazwie
            // dodajemy nowa liste pod podana nazwa produktu
            products.put(product.getName(), new ProductList());
        }

        // do podanej listy dodajemy produkt
        products.get(product.getName()).addProduct(new ProductQuantity(product, quantity));
    }

    public void addProduct(int quantity, String name, double price, LocalDateTime date) {
        Product newProduct = new Product(name, price, date);

        addProduct(quantity, newProduct);
    }

    public void listProducts() {
        for (ProductList productQuantity : products.values()) {
            System.out.println("Product: " + productQuantity);
        }
    }

    public void removeProduct(int quantity, String productName) {
        if (products.containsKey(productName)) { //jeśli posiadamy produkt o tej nazwie
            // zamysłem update'u jest aktualizacja ilosci
            products.get(productName).removeProduct(quantity);
        } else { // jesli produktu nie ma
            System.out.println("Produkt nie istnieje.");
        }
    }

    public void check() {
        for (Map.Entry<String, ProductList> entry : products.entrySet()) {
            String name = entry.getKey();

            System.out.println(name + " :");
            entry.getValue().listProductsFromThisList();


        }
    }
}
